package com.generator

/**
 * Created by a.barouski on 6/27/2017.
 */
class ClassHolder(name: String) {

    data class PropHolder(val ptype: String, val type: String, val def: String)

    val name = name
    val params = mutableMapOf<String, PropHolder>()
    val builder: StringBuilder = StringBuilder().append("data class ").append(name).append("{")

    fun addVal(name: String, type: String, def: String) {
        params.put(name, PropHolder("val", type, def))
    }

    fun addVar(name: String, type: String, def: String) {
        params.put(name, PropHolder("var", type, def))
    }

    fun build(asDataClass: Boolean): String {
        var first = true
        builder.setLength(0)
        if (asDataClass) {
            builder.append("data class $name (")
            params.forEach { t, (ptype, type, def) ->
                if (!first) builder.append(",\n") else first = false
                builder.append("$ptype $t : $type = $def")
            }
            builder.append(")")
        } else {
            builder.append("class $name {")
            params.forEach { t, (ptype, type, def) ->
                if (!first) builder.append("\n") else first = false
                builder.append("$ptype $t : $type = $def")
            }
            builder.append("}")
        }
        return builder.toString()
    }

}