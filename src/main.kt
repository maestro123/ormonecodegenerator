import com.generator.ClassHolder
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

fun main(args: Array<String>): Unit {

    val inputJsonFile: String
    val holderMap = mutableMapOf<String, ClassHolder>()

    if (args.isEmpty()) {
        inputJsonFile = "test.json"
    } else {
        inputJsonFile = args[0]
    }

    val json = JSONObject(File(inputJsonFile).readText())
    parse("test", json, holderMap)

    println("Result")
    holderMap.forEach { _, u ->
        println(u.build(false))
        File("src/com/test").mkdirs()
        File("src/com/test/" + u.name + ".kt").writeText(u.build(false))
    }
}

fun parse(name: String, json: JSONObject, holders: MutableMap<String, ClassHolder>): ClassHolder {
    val holder = ClassHolder(name)
    for (key in json.keys()) {
        val obj = json[key as String?]
        when (obj) {
            is JSONObject -> {
                println("object: " + key)
                val objHolder = parse(key as String, obj, holders)
                holder.addVal(objHolder.name.toLowerCase(), "${objHolder.name}?", "null")
            }
            is JSONArray -> {
                if (obj.length() > 0) {
                    val type = getPrimitive(obj[0])
                    if (type != null) {
                        holder.addVal(key as String, "Array<${type[0]}>?", "null")
                    } else {
                        val objHolder = parse(key as String, obj[0] as JSONObject, holders)
                        holder.addVal(objHolder.name.toLowerCase(), "Array<${objHolder.name}>?", "null")
                    }
                }
                println("array: " + key)
            }
            else -> {
                val type = getPrimitive(obj)
                if (type != null) {
                    holder.addVal(key as String, "${type[0]}?", type[1])
                }
            }
        }
    }
    holders.put(name, holder)
    return holder
}

fun getPrimitive(obj: Any): Array<String>? {
    when (obj) {
        is String -> return arrayOf("String", "null")
        is Double -> return arrayOf("Double", "0.0")
        is Int -> return arrayOf("Long", "0")
        is Long -> return arrayOf("Long", "0")
        is Boolean -> return arrayOf("Boolean", "false")
        is Float -> return arrayOf("Double", "0.0")
    }
    return null
}